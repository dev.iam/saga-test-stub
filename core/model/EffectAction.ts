import { Effect } from 'redux-saga/effects';

export class EffectAction {
    constructor(
        public yielded: Effect,
        public nextValue?: any,
        public diff?: string,
        public note?: string,
    ) { }
}
