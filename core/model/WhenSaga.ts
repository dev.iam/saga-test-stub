import { call, select, Tail } from 'redux-saga/effects';
import { SagaStub } from './SagaStub';
import { WhenEffect } from './types';

export class WhenSaga {
    constructor(private stub: SagaStub) { }

    yields(effect: WhenEffect) {
        return new SagaStubAnswers(this.stub, effect);
    }

    selects<Fn extends (state: any, ...args: any[]) => any>(selector: Fn, ...args: Tail<Parameters<Fn>>) {
        return this.yields(select(selector, ...args));
    }

    calls<Fn extends (...args: any[]) => any>(fn: Fn, ...args: Parameters<Fn>) {
        return this.yields(call(fn, ...args));
    }
}

class SagaStubAnswers {
    constructor(private stub: SagaStub, private effect: WhenEffect) { }

    doNext(...values: any[]) {
        this.stub.add(this.effect, values);
    }

    integrate(saga: SagaStub) {
        this.stub.add(this.effect, saga);
    }

    throw(error: Error) {
        this.stub.add(this.effect, error);
    }
}
