import { Effect } from 'redux-saga/effects';
import { EffectAction } from './EffectAction';

export type EffectAsArgumentMatcher = (effect: Effect) => boolean;
export type ExpectedEffect = Effect | EffectAsArgumentMatcher;
export type WhenEffect = ExpectedEffect;
export type ObjectOrFunction = any | ((arg: any) => boolean);

export type SagaRunResult = {
    actions?: EffectAction[];
    returnedValue?: any;
    error?: any;
};

