import * as _ from 'lodash';
import { Saga } from 'redux-saga';
import { Effect } from 'redux-saga/effects';
import { integrateStubProcessor, stubEffectProcessor, throwErrorProcessor } from '../factory';
import { YieldedEffectProcessor } from '../Processors';
import { SagaRunner } from '../SagaRunner';
import { EffectAsArgumentMatcher } from './types';

export class SagaStub {
    private effectProcessors: YieldedEffectProcessor[] = [];

    constructor(private saga: Saga, private args: any[], private runner: SagaRunner) { }

    getProcessors(): YieldedEffectProcessor[] {
        return _.cloneDeep(this.effectProcessors);
    }

    add(effect: Effect | EffectAsArgumentMatcher, value: any[] | SagaStub | Error) {
        this.effectProcessors = this.effectProcessors.filter(stub => !stub.handles(effect));
        if (value instanceof SagaStub) this.effectProcessors.push(integrateStubProcessor(effect, value))
        else if (value instanceof Error) this.effectProcessors.push(throwErrorProcessor(effect, value));
        else this.effectProcessors.push(stubEffectProcessor(effect, value));
    }

    getIterator(): Iterator<Effect, any> {
        return this.saga(...this.args);
    }

    run() {
        const result = this.runner.run(this);
        if (result.error) throw result.error;
        return result.returnedValue;
    }
}
