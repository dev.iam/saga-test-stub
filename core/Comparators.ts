import { injectable } from 'inversify';
import { diff } from 'jest-matcher-utils';
import _ from 'lodash';
import { ObjectOrFunction } from './model/types';


export type ComparisonResult = {
    apply: boolean;
    match?: boolean;
    diff?: string;
};

export interface Comparator {
    compare(actual: any, expected: ObjectOrFunction): ComparisonResult;

    getWeight(): number;
}

@injectable()
export class FunctionComparator implements Comparator {
    compare(actual: any, expected: ObjectOrFunction): ComparisonResult {
        if (expected instanceof Function)
            return {
                match: expected(actual),
                apply: true,
                diff: 'cannot display diff: expected is a function'
            };
        return { apply: false };
    }

    getWeight(): number {
        return 200;
    }
}

@injectable()
export class ObjectComparator implements Comparator {
    constructor() {
    }

    compare(actual: any, expected: ObjectOrFunction): ComparisonResult {
        return {
            apply: true,
            match: _.isEqual(actual, expected),
            diff: diff(actual, expected) || undefined
        };
    }

    getWeight(): number {
        return 100;
    }
}
