import { matcher } from "../factory";
import { Matcher } from "../Matcher";
import { ExpectedEffect, SagaRunResult } from "../model/types";
import { UnexpectedError } from "../SagaTester";
import { Assertion } from "./Assertion";

class DoneAfterAssert extends Assertion {
    private message: string | undefined;

    constructor(
        private expectedEffect: ExpectedEffect,
        private matcher: Matcher
    ) { super(); }

    protected getErrorMessage(inverse: boolean): string {
        if (this.message) return this.message;
        return inverse ? "Unexpected effect was yield last" : "Last yielded effect not expected";
    }

    isSuccess(result: SagaRunResult): boolean {
        const { actions = [], error } = result;
        if (error) {
            error.message = "Saga not done, an error was thrown: " + error.message
            throw new UnexpectedError(error);
        }
        const { matcher, expectedEffect } = this;
        const lastAction = actions[actions.length - 1];
        const lastYielded = lastAction.yielded;
        const comparison = matcher.compare(lastYielded, expectedEffect);
        if (!comparison.match) {
            lastAction.diff = comparison.diff;
            return false;
        }
        return true;
    }
}

export const toBeDoneAfter = (effect: ExpectedEffect) => {
    return new DoneAfterAssert(effect, matcher());
};
