import { matcher } from "../factory";
import { Matcher } from "../Matcher";
import { ObjectOrFunction, SagaRunResult } from "../model/types";
import { UnexpectedError } from "../SagaTester";
import { Assertion } from "./Assertion";

export const toReturn = (value: ObjectOrFunction) => {
    return new ReturnAssert(value, matcher());
};
class ReturnAssert extends Assertion {
    constructor(
        private expectReturn: any,
        private matcher: Matcher
    ) { super(); }

    protected getErrorMessage(inverse: boolean, returned: any): string {
        return inverse
            ? 'Unexpected value was returned'
            : 'Expected value was not returned\n' + this.matcher.compare(returned, this.expectReturn).diff;
    }

    isSuccess(result: SagaRunResult): boolean {
        const { error } = result;
        if (error) {
            error.message = "Saga didn't return a value: an error was thrown: " + error.message
            throw new UnexpectedError(error);
        }
        const { expectReturn, matcher } = this;
        const comparison = matcher.compare(result.returnedValue, expectReturn);
        return comparison.match;
    }
}
