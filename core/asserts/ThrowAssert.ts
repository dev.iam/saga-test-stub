import { SagaRunResult } from "../model/types";
import { Assertion } from "./Assertion";

export const toThrowError = (message: string) => {
    return new ThrowAssert(message);
};
class ThrowAssert extends Assertion {
    protected getErrorMessage(inverse: boolean): string {
        return inverse ? 'Unexpected error was thrown' : 'Expected error was not thrown';
    }

    constructor(private expectedError: string) { super(); }

    isSuccess(result: SagaRunResult): boolean {
        const thrownError = result.error?.message;
        const { expectedError } = this;
        return expectedError == thrownError;
    }
}
