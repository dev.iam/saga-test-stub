import { matcher } from "../factory";
import { Matcher } from "../Matcher";
import { ExpectedEffect, SagaRunResult } from "../model/types";
import { Assertion } from "./Assertion";

export const toYieldStrict = (...effects: ExpectedEffect[]) => {
    return new YieldStrictAssert(effects, matcher());
};

class YieldStrictAssert extends Assertion {
    constructor(
        private expectedEffects: ExpectedEffect[],
        private matcher: Matcher
    ) { super(); }

    protected getErrorMessage(inverse: boolean): string {
        return inverse ? 'Unexpected effect was yielded' : 'Expected effect was not yielded';
    }

    isSuccess(result: SagaRunResult): boolean {
        const { actions = [] } = result;
        const { expectedEffects, matcher } = this;
        const firstExpectedEffect = expectedEffects[0];
        const firstMatchIndex = actions.findIndex(action => matcher.compare(action.yielded, firstExpectedEffect).match);
        if (firstMatchIndex == -1) return false;
        for (let index = 0; index < expectedEffects.length; index++) {
            const expected = expectedEffects[index];
            const action = actions[index + firstMatchIndex];
            const actual = action.yielded;
            const comparison = matcher.compare(actual, expected);
            if (comparison.match) {
                action.note = "Expected effect yielded";
            }
            else {
                action.diff = comparison.diff;
                return false;
            }
        }
        return true;
    }
}
