import { matcher } from "../factory";
import { Matcher } from "../Matcher";
import { ExpectedEffect, SagaRunResult } from "../model/types";
import { Assertion } from "./Assertion";

export function toYield(...effects: ExpectedEffect[]): YieldAssert {
    return new YieldAssert(effects, matcher());
}
class YieldAssert extends Assertion {
    protected getErrorMessage(inverse: boolean): string {
        return inverse ? 'An unexpected effect was yielded' : 'Expected effects were not yielded';
    }
    constructor(
        private expectedEffects: ExpectedEffect[],
        private matcher: Matcher
    ) {
        super();
    }

    isSuccess(result: SagaRunResult): boolean {
        const { actions = [] } = result;
        const { matcher, expectedEffects: effects } = this;
        actions.
            forEach(action => {
                const { yielded: yielded } = action;
                const comparison = matcher.compare(yielded, effects[0]);
                if (comparison.match) {
                    effects.shift();
                    action.note = "Expected effect";
                }
            });

        return (effects.length == 0);
    }
}
