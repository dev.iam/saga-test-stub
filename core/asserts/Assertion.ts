import { SagaRunResult } from "../model/types";

export const not = (qualifier: Assertion): Assertion => {
    return qualifier.not();
};
export abstract class Assertion {
    private inverse: boolean = false;
    constructor() { }

    public assert(result: SagaRunResult) {
        const success = this.isSuccess(result);
        if (success == this.inverse) {
            const message = this.getErrorMessage(this.inverse, result.returnedValue) || 'Unknown failure';
            throw new Error(message);
        }
    }

    protected abstract getErrorMessage(inverse: boolean, returned?: any): string;

    protected abstract isSuccess(result: SagaRunResult): boolean;

    not(): Assertion {
        this.inverse = !this.inverse;
        return this;
    }
}
