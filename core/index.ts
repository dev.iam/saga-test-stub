import { Saga } from 'redux-saga';
import { WhenSaga } from "./model/WhenSaga";
import { SagaStub } from './model/SagaStub';
import { runner, errorBuilder, matcher } from './factory';
import { SagaTester } from './SagaTester';

export { SagaStub } from './model/SagaStub';
export { not } from './asserts/Assertion'
export { toBeDoneAfter } from './asserts/DoneAfterAssert'
export { toReturn } from './asserts/ReturnAssert'
export { toThrowError } from './asserts/ThrowAssert'
export { toYield } from './asserts/YieldAssert'
export { toYieldStrict } from './asserts/YieldStrictAssert'

export function stub<T extends Saga>(saga: T, ...args: Parameters<T>): SagaStub {
    return new SagaStub(saga, args, runner());
}

export function when(sagaStub: SagaStub) {
    return new WhenSaga(sagaStub);
}

export function run(sagaStub: SagaStub) {
    return new SagaTester(sagaStub, runner(), errorBuilder(), matcher());
}