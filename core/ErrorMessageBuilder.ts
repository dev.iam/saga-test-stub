import { bgCyan, bgGray, bgGreen, bgMagenta, bgYellow } from 'chalk';
import { injectable } from "inversify";
import { stringify } from 'jest-matcher-utils';
import { EffectAction } from "./model/EffectAction";
import { SagaRunResult } from './model/types';

@injectable()
export class ErrorMessageBuilder {
    build({ actions = [], error, returnedValue: returned }: SagaRunResult, message: string, verbose: boolean) {
        const buffer: string[] = [];
        buffer.push(message);
        actions.forEach(action => {
            const { yielded, diff, note, nextValue } = action;
            buffer.push(`${bgGreen(' YIELD ')} ${stringify(yielded)}`);
            note && buffer.push(bgGray(' NOTE  ') + ' ' + note || '');
            diff && buffer.push(bgCyan(' DIFF  ') + ' ' + diff || '');
            buffer.push(`${bgMagenta(' NEXT  ')} (${nextValue ? JSON.stringify(nextValue) : ''})`);
        });
        if (error) {
            buffer.push(`${bgYellow(' THROW ')} ${error.message}\n`);
        } else {
            const value = typeof returned == 'string' ? `"${returned}"` : `\n${JSON.stringify(returned, null, 2)}`;
            buffer.push(`${bgYellow(' DONE  ')} ${value}\n`);
        }
        return buffer.join('\n');
    }
}