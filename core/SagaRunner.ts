import { injectable } from 'inversify';
import { Effect } from 'redux-saga/effects';
import { SagaStub } from '../jest';
import { EffectAction } from "./model/EffectAction";
import { SagaRunResult } from "./model/types";
import { ProcessorResult } from './Processors';

@injectable()
export class SagaRunner {
    run(saga: SagaStub): SagaRunResult {
        const iterator = saga.getIterator();
        const processors = saga.getProcessors();
        let current = iterator.next();
        const actions: EffectAction[] = [];
        try {
            while (!current.done) {
                let actual: Effect = current.value;
                const result = processors
                    .map(processor => processor.process(actual, iterator))
                    .find(result => result.applies) || {} as ProcessorResult;
                actions.push(new EffectAction(actual, result.nextValue));

                if (result.nextValue instanceof Error) {
                    // @ts-ignore
                    current = iterator.throw(result.nextValue);
                }
                else current = iterator.next(result.nextValue);
            }
            return {
                actions,
                returnedValue: current.value,
            }
        }
        catch (error: any) {
            return {
                actions,
                error,
            };
        }
    }
}
