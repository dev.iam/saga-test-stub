import { Effect } from "redux-saga/effects";
import { SagaStub } from "../jest";
import { Matcher } from "./Matcher";
import { ExpectedEffect } from "./model/types";
import { SagaRunner } from "./SagaRunner";

export type ProcessorResult = {
    nextValue?: any,
    applies: boolean
}

export interface YieldedEffectProcessor {
    handles(effect: ExpectedEffect): boolean;
    process(effect: Effect, iterator: Iterator<Effect, Effect, any>): ProcessorResult;
}

export class StubEffectProcessor implements YieldedEffectProcessor {
    constructor(private stubbed: ExpectedEffect, private values: any[], private matcher: Matcher) { }

    handles(effect: ExpectedEffect): boolean {
        return this.matcher.compare(effect, this.stubbed).match;
    }

    process(effect: Effect, _iterator: Iterator<Effect, Effect, any>): ProcessorResult {
        if (this.matcher.compare(effect, this.stubbed).match) {
            const value = this.values.length == 1 ? this.values[0] : this.values.shift();
            return {
                nextValue: value,
                applies: true,
            }
        }
        return { applies: false };
    }
}


export class IntegrateStubProcessor implements YieldedEffectProcessor {
    constructor(private stubbed: ExpectedEffect, private saga: SagaStub, private runner: SagaRunner, private matcher: Matcher) { }

    handles(effect: ExpectedEffect): boolean {
        return this.matcher.compare(effect, this.stubbed).match;
    }

    process(effect: Effect, _iterator: Iterator<Effect, Effect, any>): ProcessorResult {
        if (this.matcher.compare(effect, this.stubbed).match) {
            return {
                nextValue: this.runner.run(this.saga).returnedValue,
                applies: true,
            }
        }
        return { applies: false };
    }
}

export class ThrowErrorProcessor implements YieldedEffectProcessor {
    constructor(private stubbed: ExpectedEffect, private error: Error, private matcher: Matcher) { }

    handles(effect: ExpectedEffect): boolean {
        return this.matcher.compare(effect, this.stubbed).match;
    }

    process(effect: Effect, _iterator: Iterator<Effect, Effect, any>): ProcessorResult {
        if (this.matcher.compare(effect, this.stubbed).match) {
            return {
                nextValue: this.error,
                applies: true,
            }
        }
        return { applies: false };
    }
}
