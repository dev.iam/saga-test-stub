import { Assertion } from "./asserts/Assertion";
import { toReturn } from "./asserts/ReturnAssert";
import { toYield } from "./asserts/YieldAssert";
import { ErrorMessageBuilder } from "./ErrorMessageBuilder";
import { Matcher } from "./Matcher";
import { SagaStub } from "./model/SagaStub";
import { ExpectedEffect, ObjectOrFunction } from "./model/types";
import { SagaRunner } from "./SagaRunner";

export class SagaTester {
    constructor(private saga: SagaStub, private runner: SagaRunner, private errorBuilder: ErrorMessageBuilder, private matcher: Matcher) { }

    private _verbose: boolean = false;

    /**
     * use with qualifiers: toYield(), toBeDoneAfter(), toYieldStrict(), toReturn(), toThrowError()
     * can be  wrapped with not(qualifier)
     */
    expecting(qualifier: Assertion): void;
    expecting(...effects: ExpectedEffect[]): void;
    expecting(qualifierOrEffects: any): void {
        let qualifier: Assertion;
        if (qualifierOrEffects instanceof Assertion) {
            qualifier = qualifierOrEffects
        }
        else {
            qualifier = toYield(...this.getEffectFromArguments(arguments));
        }
        this._expecting(qualifier);
    }
    private _expecting(qualifier: Assertion): void {
        const { _verbose: verbose, errorBuilder } = this;
        const result = this.runner.run(this.saga);
        try {
            qualifier.assert(result);
        } catch (error: any) {
            if (error instanceof UnexpectedError) {
                error.message = errorBuilder.build(result, error.message, verbose)
                throw error;
            }
            const { message } = error;
            throw new Error(errorBuilder.build(result, message, verbose));
        }
    };
    private getEffectFromArguments(args: any) {
        const effects = [];
        for (const key in args) {
            if (Object.prototype.hasOwnProperty.call(args, key)) {
                effects.push(args[key]);
            }
        }
        return effects;
    }

    expectingDone(expectReturn?: ObjectOrFunction) {
        this._expecting(toReturn(expectReturn));
    }
}

export class UnexpectedError extends Error {
    constructor(error: Error) {
        super(error.message);
        this.stack = error.stack;
        this.name = error.name;
    }
}