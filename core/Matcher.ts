import { injectable, multiInject } from 'inversify';
import { Comparator } from './Comparators';
import { ObjectOrFunction } from './model/types';

@injectable()
export class Matcher {
    constructor(
        @multiInject('Comparator') private comparators: Comparator[]
    ) {
        comparators.sort((a, b) => b.getWeight() - a.getWeight())
    }

    compare(actual: any, expected: ObjectOrFunction): MatcherResult {
        for (const comparator of this.comparators) {
            const result = comparator.compare(actual, expected);
            if (result.apply) {
                return { match: result?.match || false, diff: result?.diff };
            }
        }
        return { match: false };
    }
}

type MatcherResult = {
    match: boolean
    diff?: string
}
