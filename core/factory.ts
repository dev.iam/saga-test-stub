import { Container } from "inversify";
import "reflect-metadata";
import { SagaStub } from "../jest";
import { Comparator, FunctionComparator, ObjectComparator } from "./Comparators";
import { ErrorMessageBuilder } from "./ErrorMessageBuilder";
import { Matcher } from "./Matcher";
import { ExpectedEffect } from "./model/types";
import {
    IntegrateStubProcessor,
    StubEffectProcessor,
    ThrowErrorProcessor
} from './Processors';
import { SagaRunner } from "./SagaRunner";

export const container = new Container({ autoBindInjectable: true });
container.bind<Comparator>("Comparator").to(FunctionComparator)
container.bind<Comparator>("Comparator").to(ObjectComparator)

export function errorBuilder() {
    return container.resolve(ErrorMessageBuilder);
}

export function runner() {
    return container.resolve(SagaRunner);
}

export function matcher(): Matcher {
    return container.resolve(Matcher);
}

export function stubEffectProcessor(effect: ExpectedEffect, values: any[]) {
    return new StubEffectProcessor(effect, values, matcher())
}

export function integrateStubProcessor(effect: ExpectedEffect, saga: SagaStub) {
    return new IntegrateStubProcessor(effect, saga, runner(), matcher());
}

export function throwErrorProcessor(effect: ExpectedEffect, error: Error) {
    return new ThrowErrorProcessor(effect, error, matcher());
}
