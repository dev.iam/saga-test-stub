import { call, Effect, put, select } from 'redux-saga/effects';
import { run, SagaStub, stub, toReturn, toYield, when } from 'saga-test-stub';
import { fritkotSaga, getFritkot, getSauces, isPlateEmpty } from 'app';

describe('fritkot saga', () => {
  let saga: SagaStub;
  const fritkot: any = {};
  let hotSauces: string[];

  beforeEach(() => {
    saga = stub(fritkotSaga);
    when(saga).yields(select(getFritkot)).doNext(fritkot)
    hotSauces = [];
    when(saga).yields(call(getSauces, true)).doNext(hotSauces)
    when(saga).yields((effect: Effect) => effect.type == "CALL" && effect.payload.fn == getSauces && effect.payload.args[0] == false).doNext(['Bearnaise', 'Andalouse'])
    when(saga).yields(select(isPlateEmpty)).doNext(true);
  });

  describe("when sadly closed", () => {
    beforeEach(() => {
      fritkot.open = false;
    });

    it("should wait", () => {
      run(saga).expecting(toYield(put({ type: 'Attendre' })));
    });
  });

  describe("when open for business", () => {
    beforeEach(() => {
      fritkot.open = true;
    });

    describe("when world is sad and there is no more hot sauces", () => {
      beforeEach(() => {
        hotSauces = []
      });

      it("should ask for non hot sauce and pick the first one", () => {
        run(saga).expecting(toYield((value: any) => (value.type == 'PUT' && value.payload.action.type == 'Bearnaise')));
      });
    });

    describe("when there is Samourai sauce", () => {
      beforeEach(() => {
        hotSauces.push('Piri-piri');
        hotSauces.push('Samourai');
      });

      it("should ask for Samourai", () => {
        run(saga).expecting(toYield(put(({ type: 'Samourai' }))));
      });
    });

    describe("when there is not Samourai sauce", () => {
      beforeEach(() => {
        hotSauces.push('Piri-piri');
        hotSauces.push('Merken');
      });

      it("should ask for the second one", () => {
        run(saga).expecting(toYield(put({ type: 'Merken' })));
      });
    });

    describe("when there is 2 fries left", () => {
      beforeEach(() => {
        when(saga).yields(select(isPlateEmpty)).doNext(false, false, true);
      });

      it("should eat the fries and be sad", () => {
        run(saga).expecting(toYield(
          put({ type: 'Mange une frite' }),
          put({ type: 'Mange une frite' }),
          put({ type: "Snif ! Y'a pu d'frite" })))
      });
    });

    it("should ask for the bill and thank the chef", () => {
      run(saga).expecting(toReturn({
        ask: "L'addition",
        say: "Merci"
      }));
    });

    it("should thank the chef", () => {
      run(saga).expecting(toReturn((value: any) => value.say == 'Merci'));
    });
  });
});
