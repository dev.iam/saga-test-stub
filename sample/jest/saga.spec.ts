import { fritkotSaga, getFritkot, getSauces, isPlateEmpty } from 'app';
import { call, put, select } from 'redux-saga/effects';
import { SagaStub, stub } from 'saga-test-stub/jest';
import { when } from 'saga-test-stub/jest-when';

when(jest.fn()).calledWith('is it a sagaStub?').mockReturnValue('nop');
when(stub(fritkotSaga)).yields(put({ type: 'is it a jest.mockInstance?' })).doNext('nop');

describe('fritkot saga with jest', () => {
    let saga: SagaStub;
    const fritkot: any = {};
    let hotSauces: string[];

    beforeEach(() => {
        saga = stub(fritkotSaga);
        when(saga).selects(getFritkot).doNext(fritkot)
        hotSauces = [];
        when(saga).calls(getSauces, true).doNext(hotSauces)
        when(saga).yields(expect.objectContaining(
            {
                type: 'CALL',
                payload: expect.objectContaining({
                    fn: getSauces,
                    args: [false]
                })
            }
        )).doNext(['Bearnaise', 'Andalouse'])
        when(saga).selects(isPlateEmpty).doNext(true);
    });

    describe("when sadly closed", () => {
        beforeEach(() => {
            fritkot.open = false;
        });

        it("should wait", () => {
            expect(saga).toPut({ type: 'Attendre' });
        });

        it("should be done", () => {
            expect(saga).toReturn();
        });
    });

    describe("when open for business", () => {
        beforeEach(() => {
            fritkot.open = true;
        });

        describe("when world is sad and there is no more hot sauces", () => {
            beforeEach(() => {
                hotSauces = []
            });

            it("should ask for non hot sauce and pick the first one", () => {
                expect(saga).toYield((value: any) => (value.type == 'PUT' && value.payload.action.type == 'Bearnaise'));
            });
        });

        describe("when there is Samourai sauce", () => {
            beforeEach(() => {
                hotSauces.push('Piri-piri');
                hotSauces.push('Samourai');
            });

            it("should ask for Samourai", () => {
                expect(saga).toPut({ type: 'Samourai' });
            });
        });

        describe("when there is not Samourai sauce", () => {
            beforeEach(() => {
                hotSauces.push('Piri-piri');
                hotSauces.push('Merken');
            });

            it("should ask for the second one", () => {
                expect(saga).toYield(expect.objectContaining(
                    {
                        type: 'PUT',
                        payload: expect.objectContaining({ action: { type: 'Merken' } })
                    }));
            });
        });

        describe("when there is 2 fries left", () => {
            beforeEach(() => {
                when(saga).yields(select(isPlateEmpty)).doNext(false, false, true);
            });

            it("should eat the fries and be sad", () => {
                expect(saga).toPut(
                    { type: 'Mange une frite' },
                    { type: 'Mange une frite' },
                    { type: "Snif ! Y'a pu d'frite" })
            });
        });

        it("should ask for the bill and thank the chef", () => {
            expect(saga).toReturn({
                ask: "L'addition",
                say: "Merci"
            });
        });

        it("should ask the bill", () => {
            expect(saga).toReturn(expect.objectContaining({
                ask: "L'addition",
            }));
        });

        it("should thank the chef", () => {
            expect(saga).toReturn((value: any) => value.say == 'Merci');
        });
    });
});

