import { select, put, call } from "redux-saga/effects";

export const getFritkot = (state: any) => state.fritkot;
export const isPlateEmpty = (state: any) => state.emptyPlate;
export const getSauces = (hot: any) => { return [] };

export function* fritkotSaga(): any {
    const fritkot = yield select(getFritkot);
    if (fritkot.open) {
        yield put({ type: 'Frites' });
        const hotSauces = yield call(getSauces, true);
        if (hotSauces.length == 0) {
            const sauces = yield call(getSauces, false);
            yield put({ type: sauces[0] });
        }
        else {
            if (hotSauces.includes('Samourai')) {
                yield put({ type: 'Samourai' });
            }
            else {
                yield put({ type: hotSauces[1] });
            }
        }
        while (true) {
            if (!(yield select(isPlateEmpty))) {
                yield put({ type: 'Mange une frite' });
            }
            else {
                yield put({ type: "Snif ! Y'a pu d'frite" });
                break;
            }
        }
        return {
            ask: "L'addition",
            say: "Merci"
        };
    }
    else {
        yield put({ type: 'Attendre' });
    }
}
