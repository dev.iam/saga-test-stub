"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.fritkotSaga = exports.getSauces = exports.isPlateEmpty = exports.getFritkot = void 0;
const effects_1 = require("redux-saga/effects");
const getFritkot = (state) => state.fritkot;
exports.getFritkot = getFritkot;
const isPlateEmpty = (state) => state.emptyPlate;
exports.isPlateEmpty = isPlateEmpty;
const getSauces = (hot) => { return []; };
exports.getSauces = getSauces;
function* fritkotSaga() {
    const fritkot = yield (0, effects_1.select)(exports.getFritkot);
    if (fritkot.open) {
        yield (0, effects_1.put)({ type: 'Frites' });
        const hotSauces = yield (0, effects_1.call)(exports.getSauces, true);
        if (hotSauces.length == 0) {
            const sauces = yield (0, effects_1.call)(exports.getSauces, false);
            yield (0, effects_1.put)({ type: sauces[0] });
        }
        else {
            if (hotSauces.includes('Samourai')) {
                yield (0, effects_1.put)({ type: 'Samourai' });
            }
            else {
                yield (0, effects_1.put)({ type: hotSauces[1] });
            }
        }
        while (true) {
            if (!(yield (0, effects_1.select)(exports.isPlateEmpty))) {
                yield (0, effects_1.put)({ type: 'Mange une frite' });
            }
            else {
                yield (0, effects_1.put)({ type: "Snif ! Y'a pu d'frite" });
                break;
            }
        }
        return {
            ask: "L'addition",
            say: "Merci"
        };
    }
    else {
        yield (0, effects_1.put)({ type: 'Attendre' });
    }
}
exports.fritkotSaga = fritkotSaga;
