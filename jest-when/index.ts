import { when as jestWhen, WhenMock } from "jest-when";
import { WhenSaga } from "../core/model/WhenSaga";
import { SagaStub, when as sagaWhen } from "../jest";

type JestMock<T, Y extends any[]> = ((...args: Y) => T) | jest.MockInstance<T, Y>;
type JestMockOrSagaStub = ((...args: any) => any) | jest.MockInstance<any, any> | SagaStub;

export function when<T extends JestMockOrSagaStub>(something: T): T extends JestMock<infer T, infer Y> ? WhenMock<T, Y> : WhenSaga;
export function when<T extends JestMockOrSagaStub>(something: T) {
    if (something instanceof SagaStub) return sagaWhen(something);
    return jestWhen(something);
}