import { injectable } from 'inversify';
import { Action } from 'redux';
import { call, Effect, put } from 'redux-saga/effects';
import { Comparator, ComparisonResult } from "../core/Comparators";
import { container } from '../core/factory';
import { EffectAsArgumentMatcher, ExpectedEffect, ObjectOrFunction } from '../core/model/types';
import { SagaStub } from '../core/model/SagaStub';
import { matcher } from './factory';

export * from '../core/index';
export { };

type CallFn = (...args: any[]) => any;

expect.extend({
    toYield(saga: SagaStub, ...effects: ExpectedEffect[]) {
        return matcher().expecToYield(saga, effects, { verbose: this.expand, inverse: this.isNot })
    },
    toYieldStrict(saga: SagaStub, ...effects: ExpectedEffect[]) {
        return matcher().expecToYield(saga, effects, { verbose: this.expand, inverse: this.isNot, strict: true })
    },
    toYieldLast(saga: SagaStub, ...effects: ExpectedEffect[]) {
        return matcher().expecToYield(saga, effects, { verbose: this.expand, inverse: this.isNot, last: true })
    },
    toPut(saga: SagaStub, ...actions: Action[]) {
        const effects = actions.map(action => put(action));
        return matcher().expecToYield(saga, effects, { verbose: this.expand, inverse: this.isNot })
    },
    toCall(saga: SagaStub, fn: CallFn, ...args: Parameters<CallFn>) {
        return matcher().expecToYield(saga, [call(fn, ...args)], { verbose: this.expand, inverse: this.isNot })
    },
    toBeDone(saga: SagaStub, expected: ObjectOrFunction) {
        return matcher().expectToReturn(saga, expected, { verbose: this.expand, inverse: this.isNot })
    },
    toReturn(saga: SagaStub, expected: ObjectOrFunction) {
        return matcher().expectToReturn(saga, expected, { verbose: this.expand, inverse: this.isNot })
    },
    toBeDoneAfter(saga: SagaStub, effect: ExpectedEffect) {
        return matcher().toBeDoneAfter(saga, effect, { verbose: this.expand, inverse: this.isNot });
    }
})

declare global {
    namespace jest {
        interface Matchers<R> {
            toYield(...effects: ExpectedEffect[]): CustomMatcherResult;
            toYieldStrict(...effects: ExpectedEffect[]): CustomMatcherResult;
            toPut<A extends Action>(...actions: A[]): CustomMatcherResult;
            toCall(fn: CallFn, ...args: Parameters<CallFn>): CustomMatcherResult;
            toYieldLast(...effects: ExpectedEffect[]): CustomMatcherResult;
            toBeDoneAfter(effect: ExpectedEffect): CustomMatcherResult;
            toReturn(expectedValue: ObjectOrFunction): CustomMatcherResult;
            toBeDone(expectedValue?: any | EffectAsArgumentMatcher): CustomMatcherResult;
        }
    }
}


@injectable()
export class JestEffectAsObjectComparator implements Comparator {
    constructor() {
    }

    compare(actual: Effect<any, any>, expected: Function | Effect<any, any>): ComparisonResult {
        try {
            expect(actual).toEqual(expected);
            return {
                apply: true,
                match: true,
            }
        } catch (error: any) {
            return {
                apply: true,
                match: false,
                diff: error.matcherResult.message
            }
        }
    }

    getWeight(): number {
        return 150;
    }
}

container.bind<Comparator>("Comparator").to(JestEffectAsObjectComparator)
