import 'reflect-metadata';
import { container } from '../core/factory';
import { JestCustomMatchers } from './JestCustomMatchers';

export function matcher() {
    return container.resolve(JestCustomMatchers);
}
