import { injectable } from "inversify";
import { not, run, toYieldStrict } from "../core";
import { Assertion } from "../core/asserts/Assertion";
import { toBeDoneAfter } from "../core/asserts/DoneAfterAssert";
import { toReturn } from "../core/asserts/ReturnAssert";
import { toYield } from "../core/asserts/YieldAssert";
import { SagaStub } from "../core/model/SagaStub";
import { ExpectedEffect, ObjectOrFunction } from "../core/model/types";
import { UnexpectedError } from "../core/SagaTester";

class Options {
    inverse?: boolean = false;
    verbose?: boolean = false;
    last?: boolean = false;
    strict?: boolean = false;
}

@injectable()
export class JestCustomMatchers {
    expecToYield = (saga: SagaStub, effects: ExpectedEffect[], { inverse, last, verbose, strict }: Options): jest.CustomMatcherResult => {
        try {
            let tester = run(saga);
            let assertion: Assertion = (last) ? toBeDoneAfter(effects[effects.length - 1]) : strict ? toYieldStrict(...effects) : toYield(...effects);
            if (inverse) assertion = not(assertion);
            tester.expecting(assertion);
            return {
                pass: true != inverse,
                message: () => ''
            }

        } catch (error: any) {
            if (error instanceof UnexpectedError) throw error;
            return {
                pass: false != inverse,
                message: () => error.message
            }
        }
    }

    toBeDoneAfter(saga: SagaStub, effect: ExpectedEffect, { inverse, verbose }: Options): jest.CustomMatcherResult {
        try {
            let tester = run(saga);
            let assertion: Assertion = toBeDoneAfter(effect);
            if (inverse) assertion = not(assertion);
            tester.expecting(assertion);
            return {
                pass: true != inverse,
                message: () => ''
            }

        } catch (error: any) {
            if (error instanceof UnexpectedError) throw error;
            return {
                pass: false != inverse,
                message: () => error.message
            }
        };
    }

    expectToReturn = (saga: SagaStub, expectReturn: ObjectOrFunction, { inverse, verbose }: Options): jest.CustomMatcherResult => {
        try {
            let tester = run(saga);
            if (inverse) tester.expecting(not(toReturn(expectReturn)));
            else tester.expecting(toReturn(expectReturn));
            return {
                pass: true != inverse,
                message: () => ''
            }

        } catch (error: any) {
            if (error instanceof UnexpectedError) throw error;
            return {
                pass: false != inverse,
                message: () => error.message
            }
        }
    }
}