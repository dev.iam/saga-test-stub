import { call, Effect, put, select } from "redux-saga/effects";
import { SagaStub, stub } from "../jest";
import { when } from "../jest-when";
import { mockMe } from "./test-util";

jest.mock('./test-util')
when(mockMe).calledWith('ok').mockReturnValue('done');

const selector = (state: any) => state;

function* bSaga(): any {
    return yield select(selector)
}

function* itSaga(): Iterator<Effect, string> {
    const a = yield select();
    yield put({ type: a });
    if (a == "stop") {
        return 'stop';
    }
    const b = yield call(bSaga);
    yield put({ type: b, data: 'abc' });
    return mockMe('ok');
}

let saga: SagaStub;
let bStub: SagaStub;

beforeEach(() => {
    saga = stub(itSaga);
    bStub = stub(bSaga);
});

it("should work", () => {
    when(saga).yields((eff: Effect) => eff.type == 'SELECT').doNext('hola');
    when(bStub).selects(selector).doNext('chao');
    when(saga).calls(bSaga).integrate(bStub);

    expect(saga).toYield(effect => effect.type == "PUT" && effect.payload.action.type == 'hola');
    expect(saga).toYield(put({ type: 'hola' }));
    expect(saga).toPut({ type: 'hola' });

    expect(saga).toYieldLast(put({ type: 'chao', data: 'abc' }));

    expect(saga).toYield(
        put({ type: 'hola' }),
        put({ type: 'chao', data: 'abc' })
    );
    expect(saga).toPut(
        { type: 'hola' },
        { type: 'chao', data: 'abc' }
    );

    expect(saga).not.toYield(put({ type: 'bonjour' }));
    expect(saga).not.toPut({ type: 'bonjour' });

    expect(saga).not.toReturn('don');

    expect(saga).toReturn('done');
});

it("should work 2", () => {
    when(saga).yields(select()).doNext('stop');
    expect(saga).toPut({ type: 'stop' });
    expect(saga).toYieldLast(put({ type: 'stop' }));
});

it("should not yield", () => {
    expect(() => expect(saga).toYield(put({ type: 'hol' })))
        .toThrowError(/Expected effects were not yielded.*/);
});
it("should not done with same value", () => {
    expect(() => expect(saga).toReturn('don'))
        .toThrowError(/Expected return value was not found*/);
});

function* throwSaga() {
    try {
        yield select();
    }
    catch (e) {
        yield put({ type: 'ERROR' });
    }
}

it("should throw", () => {
    let saga = stub(throwSaga);
    when(saga).yields(select()).throw(new Error('a'));
    expect(saga).toPut({ type: 'ERROR' });
});
