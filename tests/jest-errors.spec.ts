import { fritkotSaga, getFritkot, getSauces, isPlateEmpty } from 'app';
import { call, put, select } from 'redux-saga/effects';
import "reflect-metadata";
import { SagaStub, when } from '../core';
import { stub } from '../jest';

describe('Integration error tests', () => {
    let saga: SagaStub;
    const fritkot: any = {};
    let hotSauces: string[];

    beforeEach(() => {
        hotSauces = [];

        saga = stub(fritkotSaga);
        when(saga).yields(select(getFritkot)).doNext(fritkot)
        when(saga).yields(call(getSauces, true)).doNext(hotSauces)
        when(saga).yields(call(getSauces, false)).doNext(['Bearnaise', 'Andalouse'])
        when(saga).yields(select(isPlateEmpty)).doNext(true);
    });

    describe("when close", () => {
        beforeEach(() => {
            fritkot.open = false;
        });

        it("should throw", () => {
            expect(() => expect(saga).toYield(put({ type: 'Attendr' })))
                .toThrowError(/Expected effects were not yielded.*/);
        });

        it("should throw", () => {
            fritkot.open = true;
            expect(() => expect(saga).toReturn())
                .toThrowError(/Expected return value was not found*/);
        });
    });

    describe("when open", () => {
        beforeEach(() => {
            fritkot.open = true;
        });

        it("should throw when not last", () => {
            expect(() => expect(saga).toYieldLast(
                put({ type: 'Frites' }),
            )).toThrowError(/An effect was yielded after last expected one.*/);
        });

        it("should throw", () => {
            expect(() => expect(saga).toYield(
                put({ type: 'Frites' }),
                put({ type: 'Frites' }),
            )).toThrowError(/Expected effects were not yielded.*/);
        });

        describe("when world is sad and there is no more hot sauces", () => {
            beforeEach(() => {
                hotSauces = []
            });

            it("should throw", () => {
                expect(() => {
                    expect(saga).toYield((value: any) => (value.type == 'PUT' && value.payload.action.type == 'Bearnais'));
                }).toThrowError(/Expected effects were not yielded.*/);
            });
        });

        describe("when there is Samourai sauce", () => {
            beforeEach(() => {
                hotSauces.push('Piri-piri');
                hotSauces.push('Samoura');
            });

            it("should throw", () => {
                expect(() => {
                    expect(saga).toYield(put(({ type: 'Samourai' })));
                }).toThrowError(/Expected effects were not yielded.*/);
            });
        });

        describe("when there is not Samourai sauce", () => {
            beforeEach(() => {
                hotSauces.push('Piri-piri');
                hotSauces.push('Merken');
            });

            it("should throw", () => {
                expect(() => {
                    expect(saga).toYield(put({ type: 'Merke' }));
                }).toThrowError(/Expected effects were not yielded.*/);
            });
        });

        describe("when there is 2 fries left", () => {
            beforeEach(() => {
                when(saga).yields(select(isPlateEmpty)).doNext(false, false, true);
            });

            it("should throw", () => {
                expect(() => {
                    expect(saga).toYield(
                        put({ type: 'Mange une frite' }),
                        put({ type: 'Mange une frit' }),
                        put({ type: "Snif ! Y'a pu d'frite" }))
                }).toThrowError(/Expected effects were not yielded.*/);
            });

            it("should throw when last", () => {
                expect(() => expect(saga).not.toYieldLast(
                    put({ type: "Snif ! Y'a pu d'frite" }),
                )).toThrowError(/Expected effect was the last one yielded.*/);
            });
        });

        it("should throw", () => {
            expect(() => {
                expect(saga).toReturn({
                    ask: "L'additio",
                    say: "Merci"
                });

            }).toThrowError(/Expected return value was not found*/);
        });

        it("should throw", () => {
            expect(() => {
                expect(saga).toReturn((value: any) => value.say == 'Merc');
            }).toThrowError(/Expected return value was not found*/);
        });
    });
});
