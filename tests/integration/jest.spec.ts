import { call, put, select } from "redux-saga/effects";
import { stub, SagaStub, when } from "../../jest";
import { otherSaga, saySomething, selectMessage, setMessageNumber } from "./sagas";

describe("toYield", () => {
    let saga: SagaStub;
    beforeEach(() => {
        saga = stub(saySomething, 'Nick', 'Richard', 'David', 'Syd', 'Roger');
    });
    describe("with plain effect", () => {
        beforeEach(() => {
            when(saga).selects(selectMessage).doNext('Hi');
        });
        describe("standard", () => {
            it("success", () => {
                expect(saga).toYield(
                    put({ type: 'SEND', data: 'Hi Nick!' }),
                    put({ type: 'SEND', data: 'Hi Richard!' }),
                    put({ type: 'SEND', data: 'Hi David!' }),
                    put({ type: 'SEND', data: 'Hi Syd!' }),
                    put({ type: 'SEND', data: 'Hi Roger!' }),
                );
            });
            it("failure", () => {
                expect(() =>
                    expect(saga).toYield(
                        put({ type: 'SEND', data: 'Hi Richard!' }),
                        put({ type: 'SEND', data: 'Hi Nick!' }),
                    )
                ).toThrowErrorMatchingSnapshot();
            });
        });
        describe("negate", () => {
            it("standard", () => {
                expect(saga).not.toYield(
                    put({ type: 'SEND', data: 'Hi John!' }),
                );
            });
            it("failure", () => {
                expect(() =>
                    expect(saga).not.toYield(
                        put({ type: 'SEND', data: 'Hi Roger!' }),
                    )
                ).toThrowErrorMatchingSnapshot();
            });
        });
        describe("with throw", () => {
            beforeEach(() => {
                when(saga).yields(effect => effect.type == "PUT").throw(new Error('fail'));
            });
            it("should not fail on expected before throw", () => {
                expect(saga).toYield(select(selectMessage));
            });
        });
    });

    describe("with functions", () => {
        beforeEach(() => {
            when(saga).yields(effect => effect.type == 'SELECT' && effect.payload.selector == selectMessage).doNext('Hello');
        });
        describe("standard", () => {
            it("success", () => {
                expect(saga).toYield(
                    (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello Richard!'),
                    (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello Syd!'),
                )
            });
            it("failure", () => {
                expect(() =>
                    expect(saga).toYield(
                        (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello Richard!'),
                        (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello John!'),
                    )
                ).toThrowErrorMatchingSnapshot();
            });
        });
        describe("negate", () => {
            it("success", () => {
                expect(saga).not.toYield(
                    (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello John!'),
                );
            });
            it("failure", () => {
                expect(() =>
                    expect(saga).not.toYield(
                        (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello Richard!'),
                        (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello Syd!'),
                    )
                ).toThrowErrorMatchingSnapshot();
            });
        });
    });

    describe("with expect.containing", () => {
        beforeEach(() => {
            when(saga).yields(expect.objectContaining({
                type: 'SELECT',
                payload: expect.objectContaining({
                    selector: selectMessage
                })
            })).doNext('Hello');
        });
        it("success", () => {
            expect(saga).toYield(
                expect.objectContaining({
                    type: 'PUT',
                    payload: expect.objectContaining({
                        action: {
                            type: 'SEND',
                            data: 'Hello Richard!'
                        }
                    })
                }),
                expect.objectContaining({
                    type: 'PUT',
                    payload: expect.objectContaining({
                        action: {
                            type: 'SEND',
                            data: 'Hello Syd!'
                        }
                    })
                }),
            )
        });
    });
});
describe("toReturn", () => {
    let saga: SagaStub;

    beforeEach(() => {
        saga = stub(saySomething, 'Bart');
    });

    describe("standard", () => {
        it("success", () => {
            expect(saga).toReturn({
                messages: {
                    number: 1,
                    destination: ['Bart'],
                }
            });
            expect(saga).toReturn((value: any) => value.messages.destination[0] == 'Bart');
            expect(saga).toReturn({
                messages: expect.objectContaining({ destination: ['Bart'] })
            });
        });
        it("failure", () => {
            expect(() =>
                expect(saga).toReturn({
                    messages: {
                        number: 2,
                        destination: ['Bart'],
                    }
                })
            ).toThrowErrorMatchingSnapshot();
            expect(() =>
                expect(saga).toReturn((value: any) => value.messages.destination[0] == 'Homer')
            ).toThrowErrorMatchingSnapshot();
        });
        describe("with throw", () => {
            beforeEach(() => {
                when(saga).yields(effect => effect.type == "SELECT").throw(new Error('fail'));
            });
            it("should fail with normal message", () => {
                expect(() =>
                    expect(saga).toReturn({
                        messages: {
                            number: 1,
                            destination: ['Bart'],
                        }
                    })
                ).toThrowErrorMatchingSnapshot();
            });
        });
    });
    describe("negate", () => {
        it("success", () => {
            expect(saga).not.toReturn({
                messages: {
                    number: 1,
                    destination: ['Homer'],
                }
            });
            expect(saga).not.toReturn((value: any) => value.messages.destination[0] == 'Homer');
        });
        it("failure", () => {
            expect(() =>
                expect(saga).not.toReturn({
                    messages: {
                        number: 1,
                        destination: ['Bart'],
                    }
                })
            ).toThrowErrorMatchingSnapshot();
            expect(() =>
                expect(saga).not.toReturn((value: any) => value.messages.destination[0] == 'Bart')
            ).toThrowErrorMatchingSnapshot();
        });
    });
});

it("should integrate", () => {
    const saga = stub(saySomething, 'one', 'two');
    when(saga).selects(selectMessage).doNext('Hi');

    const underTest = stub(otherSaga);
    when(underTest).calls(saySomething, 'other').integrate(saga);

    expect(underTest).toYield(call(setMessageNumber, 2));
});

it("should throw", () => {
    const underTest = stub(otherSaga);
    const error = new Error('oops');
    when(underTest).calls(saySomething, 'other').throw(error);
    expect(underTest).toYield(put({ type: 'error', error }));
});

it("toThrowError", () => {
    const saga = stub(saySomething);
    when(saga).selects(selectMessage).doNext('throw');

    expect(() => saga.run()).toThrowError('you said throw');
    expect(() =>
        expect(() => saga.run()).toThrowError('you said throw!')
    ).toThrowError();

    expect(() => saga.run()).not.toThrowError('you said throw!');
    expect(() =>
        expect(() => saga.run()).not.toThrowError('you said throw')
    ).toThrowError();
});

describe("toBeDoneAfter", () => {
    const saga: SagaStub = stub(saySomething, "Doh");
    beforeEach(() => {
        when(saga).selects(selectMessage).doNext('Homer:');
    });

    it("success", () => {
        expect(saga).toBeDoneAfter(put({ type: 'SEND', data: "Homer: Doh!" }));
        expect(saga).not.toBeDoneAfter(put({ type: 'SEND', data: "Homer: Duh!" }));
        expect(saga).toBeDoneAfter(effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Homer: Doh!');
    });

    it("fail with done", () => {
        expect(() =>
            expect(saga).toBeDoneAfter(select(selectMessage))
        ).toThrowErrorMatchingSnapshot();
        expect(() =>
            expect(saga).not.toBeDoneAfter(put({ type: 'SEND', data: "Homer: Doh!" }))
        ).toThrowErrorMatchingSnapshot();
    });

    it("fail with error thrown", () => {
        when(saga).selects(selectMessage).throw(new Error('fail'));
        expect(() =>
            expect(saga).toBeDoneAfter(select(selectMessage))
        ).toThrowErrorMatchingSnapshot();
    });
});

describe("toYieldStrict", () => {
    const saga = stub(saySomething, 'Nick', 'Richard', 'David', 'Syd', 'Roger');
    beforeEach(() => {
        when(saga).selects(selectMessage).doNext('Hi');
    });

    it("success", () => {
        expect(saga).toYieldStrict(
            put({ type: 'SEND', data: 'Hi David!' }),
            put({ type: 'SEND', data: 'Hi Syd!' }),
            put({ type: 'SEND', data: 'Hi Roger!' }),
        );
        expect(saga).toYieldStrict(
            effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hi David!',
            effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hi Syd!',
        );
        expect(saga).not.toYieldStrict(
            put({ type: 'SEND', data: 'Hi David!' }),
            put({ type: 'SEND', data: 'Hi Roger!' }),
            put({ type: 'SEND', data: 'Hi Syd!' }),
        )
    });

    it("fail", () => {
        expect(() => {
            expect(saga).toYieldStrict(
                select(selectMessage),
                put({ type: 'SEND', data: 'Hi Nick!' }),
                put({ type: 'SEND', data: 'Hi Roger!' }),
            );
        }).toThrowErrorMatchingSnapshot();
        expect(() => {
            expect(saga).toYieldStrict(
                put({ type: 'SEND', data: 'Hi Roger' }),
            );
        }).toThrowErrorMatchingSnapshot();
    });
});
