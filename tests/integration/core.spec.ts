import { call, put, select } from "redux-saga/effects";
import { not, run, SagaStub, stub, toBeDoneAfter, toReturn, toThrowError, toYield, toYieldStrict, when } from "../../core";
import { currentPosition, distanceToLine, drive, getRoute, otherSaga, saySomething, selectMessage, setMessageNumber, trafficLight } from "./sagas";

describe("toYield", () => {
    let saga: SagaStub;
    beforeEach(() => {
        saga = stub(saySomething, 'Nick', 'Richard', 'David', 'Syd', 'Roger');
    });
    describe("with plain effect", () => {
        beforeEach(() => {
            when(saga).selects(selectMessage).doNext('Hi');
        });
        describe("standard", () => {
            it("success", () => {
                run(saga).expecting(toYield(
                    put({ type: 'SEND', data: 'Hi Nick!' }),
                    put({ type: 'SEND', data: 'Hi Richard!' }),
                    put({ type: 'SEND', data: 'Hi David!' }),
                    put({ type: 'SEND', data: 'Hi Syd!' }),
                    put({ type: 'SEND', data: 'Hi Roger!' }),
                ));
            });
            it("failure", () => {
                expect(() =>
                    run(saga).expecting(toYield(
                        put({ type: 'SEND', data: 'Hi Richard!' }),
                        put({ type: 'SEND', data: 'Hi Nick!' }),
                    ))
                ).toThrowError(/Expected effects were not yielded/);
            });
        });
        describe("negate", () => {
            it("standard", () => {
                run(saga).expecting(not(toYield(
                    put({ type: 'SEND', data: 'Hi John!' }),
                )));
            });
            it("failure", () => {
                expect(() =>
                    run(saga).expecting(not(toYield(
                        put({ type: 'SEND', data: 'Hi Roger!' }),
                    )))
                ).toThrowError(/An unexpected effect was yielded/);
            });
        });
        describe("with throw", () => {
            beforeEach(() => {
                when(saga).yields(effect => effect.type == "PUT").throw(new Error('fail'));
            });
            it("should not fail on expected before throw", () => {
                run(saga).expecting(toYield(select(selectMessage)));
            });
        });
    });

    describe("with functions", () => {
        beforeEach(() => {
            when(saga).yields(effect => effect.type == 'SELECT' && effect.payload.selector == selectMessage).doNext('Hello');
        });
        describe("standard", () => {
            it("success", () => {
                run(saga).expecting(toYield(
                    (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello Richard!'),
                    (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello Syd!'),
                ))
            });
            it("failure", () => {
                expect(() =>
                    run(saga).expecting(toYield(
                        (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello John!'),
                    ))
                ).toThrowError(/Expected effects were not yielded/);
            });
        });
        describe("negate", () => {
            it("success", () => {
                run(saga).expecting(not(toYield(
                    (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello John!'),
                )));
            });
            it("failure", () => {
                expect(() =>
                    run(saga).expecting(not(toYield(
                        (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello Richard!'),
                        (effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hello Syd!'),
                    )))
                ).toThrowError(/An unexpected effect was yielded/);
            });
        });
    });
});
describe("toReturn", () => {
    let saga: SagaStub;

    beforeEach(() => {
        saga = stub(saySomething, 'Bart');
    });

    describe("standard", () => {
        it("success", () => {
            run(saga).expecting(toReturn({
                messages: {
                    number: 1,
                    destination: ['Bart'],
                }
            }));
            run(saga).expecting(toReturn((value: any) => value.messages.destination[0] == 'Bart'));
        });
        it("failure", () => {
            expect(() =>
                run(saga).expecting(toReturn({
                    messages: {
                        number: 2,
                        destination: ['Bart'],
                    }
                }))
            ).toThrowError(/Expected value was not returned/);
            expect(() =>
                run(saga).expecting(toReturn((value: any) => value.messages.destination[0] == 'Homer'))
            ).toThrowError(/Expected value was not returned/);
        });
        describe("with throw", () => {
            beforeEach(() => {
                when(saga).yields(effect => effect.type == "PUT").throw(new Error('fail'));
            });
            it("should fail with normal message", () => {
                expect(() =>
                    run(saga).expecting(toReturn({
                        messages: {
                            number: 1,
                            destination: ['Bart'],
                        }
                    }))
                ).toThrowError(/Saga didn't return a value: an error was thrown: fail/);
            });
        });
    });
    describe("negate", () => {
        it("success", () => {
            run(saga).expecting(not(toReturn({
                messages: {
                    number: 1,
                    destination: ['Homer'],
                }
            })));
            run(saga).expecting(not(toReturn((value: any) => value.messages.destination[0] == 'Homer')));
        });
        it("failure", () => {
            expect(() =>
                run(saga).expecting(not(toReturn({
                    messages: {
                        number: 1,
                        destination: ['Bart'],
                    }
                })))
            ).toThrowError(/Unexpected value was returned/);
            expect(() =>
                run(saga).expecting(not(toReturn((value: any) => value.messages.destination[0] == 'Bart')))
            ).toThrowError(/Unexpected value was returned/);
        });
    });
});

it("should integrate", () => {
    const saga = stub(saySomething, 'one', 'two');
    when(saga).selects(selectMessage).doNext('Hi');

    const underTest = stub(otherSaga);
    when(underTest).calls(saySomething, 'other').integrate(saga);

    run(underTest).expecting(toYield(call(setMessageNumber, 2)));
});

it("should throw", () => {
    const underTest = stub(otherSaga);
    const error = new Error('oops');
    when(underTest).calls(saySomething, 'other').throw(error);
    run(underTest).expecting(toYield(put({ type: 'error', error })));
});

it("toThrowError", () => {
    const saga = stub(saySomething);
    when(saga).selects(selectMessage).doNext('throw');

    run(saga).expecting(toThrowError('you said throw'));
    expect(() =>
        run(saga).expecting(toThrowError('you said throw!'))
    ).toThrowError(/Expected error was not thrown/);

    run(saga).expecting(not(toThrowError('you said throw!')));
    expect(() =>
        run(saga).expecting(not(toThrowError('you said throw')))
    ).toThrowError(/Unexpected error was thrown/);

});

describe("toBeDoneAfter", () => {
    const saga: SagaStub = stub(saySomething, "Doh");
    beforeEach(() => {
        when(saga).selects(selectMessage).doNext('Homer:');
    });

    it("success", () => {
        run(saga).expecting(toBeDoneAfter(put({ type: 'SEND', data: "Homer: Doh!" })));
        run(saga).expecting(not(toBeDoneAfter(put({ type: 'SEND', data: "Homer: Duh!" }))));
        run(saga).expecting(toBeDoneAfter(effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Homer: Doh!'));
    });

    it("fail with done", () => {
        expect(() =>
            run(saga).expecting(toBeDoneAfter(select(selectMessage)))
        ).toThrowError(/Last yielded effect not expected:*/);
        expect(() =>
            run(saga).expecting(not(toBeDoneAfter(put({ type: 'SEND', data: "Homer: Doh!" }))))
        ).toThrowError(/Unexpected effect was yield last*/);
    });

    it("fail with error thrown", () => {
        when(saga).selects(selectMessage).throw(new Error('fail'));
        expect(() => run(saga).expecting(toBeDoneAfter(select(selectMessage))))
            .toThrowError(/Saga not done, an error was thrown: fail*/);
    });
});

describe("toYieldStrict", () => {
    const saga = stub(saySomething, 'Nick', 'Richard', 'David', 'Syd', 'Roger');
    beforeEach(() => {
        when(saga).selects(selectMessage).doNext('Hi');
    });

    it("success", () => {
        run(saga).expecting(toYieldStrict(
            put({ type: 'SEND', data: 'Hi David!' }),
            put({ type: 'SEND', data: 'Hi Syd!' }),
            put({ type: 'SEND', data: 'Hi Roger!' }),
        ));
        run(saga).expecting(toYieldStrict(
            effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hi David!',
            effect => effect.type == 'PUT' && effect.payload.action.type == 'SEND' && effect.payload.action.data == 'Hi Syd!',
        ));
        run(saga).expecting(not(toYieldStrict(
            put({ type: 'SEND', data: 'Hi David!' }),
            put({ type: 'SEND', data: 'Hi Roger!' }),
            put({ type: 'SEND', data: 'Hi Syd!' }),
        )))
    });

    it("fail", () => {
        expect(() => {
            run(saga).expecting(toYieldStrict(
                select(selectMessage),
                put({ type: 'SEND', data: 'Hi Nick!' }),
                put({ type: 'SEND', data: 'Hi Roger!' }),
            ));
        }).toThrowError(/Expected effect was not yielded.*/);
        expect(() => {
            run(saga).expecting(toYieldStrict(
                put({ type: 'SEND', data: 'Hi Roger' }),
            ))
        }).toThrowError(/Expected effect was not yielded.*/);
    });
});

describe("standalone run", () => {
    let saga: SagaStub;

    beforeEach(() => {
        saga = stub(saySomething, 'Moe', 'Barney', 'Burns');
    });

    it("should return", () => {
        const result = saga.run();
        expect(result).toEqual({ messages: { destination: ['Moe', 'Barney', 'Burns'], number: 3 } });
    });
    it("should throw", () => {
        const error = new Error();
        when(saga).selects(selectMessage).throw(error);
        expect(() => saga.run()).toThrow(error);
    });
});

describe('demo', () => {
    const saga = stub(drive, 'point D');
    let lights: any;

    beforeEach(() => {
        when(saga).yields(select(currentPosition)).doNext('point A', 'point B', 'point C', 'point D');

        lights = { green: false, red: false, yellow: false }
        when(saga).selects(trafficLight).doNext(lights);
    });

    describe('when route is unknown', () => {
        beforeEach(() => {
            when(saga).yields(call(getRoute, 'point A', 'point D')).doNext('unknown');
        });

        it('should do nothing after asking for route', () => {
            run(saga).expecting(toBeDoneAfter(call(getRoute, 'point A', 'point D')));
        });

        it('should return cannot drive there', () => {
            run(saga).expecting(toReturn('ask for direction'));
        });
    });

    describe('when a route is found', () => {
        beforeEach(() => {
            when(saga).yields(call(getRoute, 'point A', 'point D')).doNext('go to B, then C and you will be at D');
        });

        describe('when traffic light is green', () => {
            beforeEach(() => {
                lights.green = true;
            });

            it('should keep driving', () => {
                run(saga).expecting(toYield(put({ type: 'keep driving' })));
            });
        });

        describe('when traffic light is yellow', () => {
            beforeEach(() => {
                lights.yellow = true;
            });

            describe('when distance is less than 1', () => {
                beforeEach(() => {
                    when(saga).selects(distanceToLine).doNext(0.9);
                });

                it('should apply brakes', () => {
                    run(saga).expecting(toYield(put({ type: 'apply brakes' })));
                });
            });
            describe('when distance is more than 1', () => {
                beforeEach(() => {
                    when(saga).selects(distanceToLine).doNext(1.1);
                });

                it('should keep driving', () => {
                    run(saga).expecting(toYield(put({ type: 'keep driving' })));
                });
            });
        });

        describe('when route goes by bermuda triangle', () => {
            beforeEach(() => {
                when(saga).yields(select(currentPosition)).doNext('point A', 'bermuda triangle', 'point D');
            });

            it('should throw an error', () => {
                run(saga).expecting(toThrowError('we are lost'));
            });
        });
    });
});