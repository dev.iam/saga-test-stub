import { call, Effect, put, select } from "redux-saga/effects";

export const selectMessage = (state: any) => state + '';

export function* saySomething(...names: string[]): Iterator<Effect, any> {
    const message = yield select(selectMessage);

    if (message == 'throw') {
        throw new Error('you said throw');
    }
    for (const name of names) {
        yield put({ type: 'SEND', data: `${message} ${name}!` });
    }

    return {
        messages: {
            number: names.length,
            destination: names,
        }
    };
}

export function setMessageNumber(length: number) {
    throw new Error('not implemented');
}

export function* otherSaga(): Iterator<Effect, any> {
    try {
        const result: any = yield call(saySomething, 'other');
        yield call(setMessageNumber, result.messages.number);
        yield select(selectMessage);
    } catch (error) {
        yield put({ type: 'error', error });
    }
}

export const getRoute = (_origin: string, _destination: string) => 'follow the yellow path';
export const trafficLight = (_: any) => ({});
export const distanceToLine = (_: any) => '';
export const currentPosition = (_: any) => '';
export const logPosition = (_: any) => '';

export function* drive(destination: string): Iterator<Effect, string, any> {
    const origin = yield select(currentPosition)
    const route = yield call(getRoute, origin, destination);
    if (route === 'unknown') {
        return 'ask for direction';
    }
    let position = origin;
    do {
        if (position == origin) {
            yield put({ type: 'at origin' });
        }
        const lights = yield select(trafficLight);
        if (lights.green == true) {
            yield put({ type: 'keep driving' });
        }
        if (lights.red == true) {
            yield put({ type: 'apply brakes' });
        }
        if (lights.yellow == true) {
            const distance = yield select(distanceToLine);
            if (distance < 1) {
                yield put({ type: 'apply brakes' });
            }
            else {
                yield put({ type: 'keep driving' });
            }
        }
        position = yield select(currentPosition);
        if (position == 'bermuda triangle') {
            throw new Error('we are lost');
        }

    } while (position != destination);
    return 'we are at destination';
}