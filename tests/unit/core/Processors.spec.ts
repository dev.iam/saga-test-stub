import { when } from "jest-when";
import { Mock, Test } from "mockito-decorator";
import { put, select } from "redux-saga/effects";
import { SagaStub } from "../../../core";
import { Matcher } from "../../../core/Matcher";
import { IntegrateStubProcessor, StubEffectProcessor } from "../../../core/Processors";
import { SagaRunner } from "../../../core/SagaRunner";

@Test
class ExpectedEffectsProcessorTest {

    @Mock(Matcher)
    matcher!: Matcher;

    handles() {
        const iterator = { next: jest.fn() };

        describe('StubEffectProcessor', () => {
            beforeEach(() => {
                when(this.matcher.compare)
                    .mockReturnValue({ match: false })
                    .calledWith(put({ type: 'a' }), select()).mockReturnValue({ match: true });
            });

            it('should return handles', () => {
                const processor = new StubEffectProcessor(select(), [''], this.matcher);
                expect(processor.handles(put({ type: 'a' }))).toEqual(true)
            });

            it('should return not handles', () => {
                const processor = new StubEffectProcessor(select(), [''], this.matcher);
                expect(processor.handles(put({ type: 'b' }))).toEqual(false)
            });

            it('should return values', () => {
                const processor = new StubEffectProcessor(select(), ['1', '2'], this.matcher);
                expect(processor.process(put({ type: 'a' }), iterator)).toEqual({
                    applies: true,
                    nextValue: '1'
                })
                expect(processor.process(put({ type: 'a' }), iterator)).toEqual({
                    applies: true,
                    nextValue: '2',
                })
            });

            it('should not return values', () => {
                const processor = new StubEffectProcessor(select(), ['1', '2'], this.matcher);
                expect(processor.process(put({ type: 'b' }), iterator)).toEqual({ applies: false })
            });
        });

        describe('IntegrateStubProcessor', () => {
            const stub = { getProcessors: jest.fn(), getIterator: jest.fn() } as unknown as SagaStub;
            const runner = { run: jest.fn() } as unknown as SagaRunner;

            beforeEach(() => {
                when(this.matcher.compare)
                    .mockReturnValue({ match: false })
                    .calledWith(put({ type: 'a' }), select()).mockReturnValue({ match: true });
            });

            it('should return handles', () => {
                const processor = new IntegrateStubProcessor(select(), stub, runner, this.matcher);
                expect(processor.handles(put({ type: 'a' }))).toEqual(true)
            });

            it('should return not handles', () => {
                const processor = new IntegrateStubProcessor(select(), stub, runner, this.matcher);
                expect(processor.handles(put({ type: 'b' }))).toEqual(false)
            });

            it('should return values', () => {
                const processor = new IntegrateStubProcessor(select(), stub, runner, this.matcher);
                when(runner.run).calledWith(stub).mockReturnValue({ returnedValue: 'ghi' })
                expect(processor.process(put({ type: 'a' }), iterator)).toEqual({
                    applies: true,
                    nextValue: 'ghi'
                })
            });

            it('should not return values', () => {
                const processor = new IntegrateStubProcessor(select(), stub, runner, this.matcher);
                expect(processor.process(put({ type: 'b' }), iterator)).toEqual({ applies: false })
            });
        });
    }
}