import "reflect-metadata";
import { when } from "jest-when";
import { delay, Effect, select } from "redux-saga/effects";
import { ObjectComparator, Comparator, FunctionComparator } from "../../../core/Comparators";
import { Matcher } from "../../../core/Matcher";
import { diff } from 'jest-matcher-utils';

jest.mock("jest-matcher-utils");

describe("Matcher", () => {
    let comparators: Comparator[];
    let underTest: Matcher;

    beforeEach(() => {
        comparators = [];
        underTest = new Matcher(comparators);
    });

    describe("with multiple comparators", () => {
        const comparator1 = { compare: jest.fn(), getWeight: jest.fn() } as Comparator;
        const comparator2 = { compare: jest.fn(), getWeight: jest.fn() } as Comparator;
        const comparator3 = { compare: jest.fn(), getWeight: jest.fn() } as Comparator;
        beforeEach(() => {
            comparators.push(comparator1);
            comparators.push(comparator2);
            comparators.push(comparator3);
        });

        it("should sort comparator by descending weight", () => {
            when(comparator1.getWeight).calledWith().mockReturnValue(-1);
            when(comparator2.getWeight).calledWith().mockReturnValue(100);
            when(comparator3.getWeight).calledWith().mockReturnValue(50);

            new Matcher(comparators);
            expect(comparators).toEqual([comparator2, comparator3, comparator1]);
        });

        describe("when two comparator apply", () => {
            beforeEach(() => {
                when(comparator1.compare).calledWith(select(), delay(1)).mockReturnValue({ apply: true, match: false, diff: 'not the same' });
                when(comparator2.compare).calledWith(select(), delay(1)).mockReturnValue({ apply: false });
                when(comparator3.compare).calledWith(select(), delay(1)).mockReturnValue({ apply: true, match: true, diff: 'same' });
            });

            it("should return match and diff from the first applying comparator", () => {
                expect(underTest.compare(select(), delay(1))).toEqual({ match: false, diff: 'not the same' });
            });
        });

        describe("when no comparator applies", () => {
            beforeEach(() => {
                when(comparator1.compare).calledWith(select(), delay(1)).mockReturnValue({ apply: false });
                when(comparator2.compare).calledWith(select(), delay(1)).mockReturnValue({ apply: false });
                when(comparator3.compare).calledWith(select(), delay(1)).mockReturnValue({ apply: false });
            });

            it("should return false and undefined diff", () => {
                expect(underTest.compare(select(), delay(1))).toEqual({ match: false });
            });
        });
    });
});

const actual = { type: 'abc' } as Effect;
const expected = { type: 'def' } as Effect;

describe("EffectAsFunctionComparator", () => {
    const underTest = new FunctionComparator();

    describe("when expected is not a function", () => {
        it("should return apply false", () => {
            expect(underTest.compare(select(), select())).toEqual({ apply: false });
        });
    });

    let returnValue: boolean;
    let calledWith: Effect = {} as Effect;
    const fn = (arg: Effect) => {
        calledWith = arg;
        return returnValue;
    };

    describe("when expected is a function returning false", () => {
        it("should return apply true, match false and undefined diff", () => {
            returnValue = false;

            expect(underTest.compare(actual, fn)).toEqual({
                match: false,
                diff: "cannot display diff: expected is a function",
                apply: true
            });
            expect(calledWith).toBe(actual);
        });
    });
    describe("when expected is a function return true", () => {
        it("should return apply true, match true and undefined diff", () => {
            returnValue = true;

            expect(underTest.compare(actual, fn)).toEqual({
                match: true,
                diff: "cannot display diff: expected is a function",
                apply: true
            });
            expect(calledWith).toBe(actual);
        });
    });
});

describe("EffectAsObjectComparator", () => {
    it("should return value from jest toEqual", () => {
        when(diff).calledWith(actual, expected).mockReturnValue('fail');

        expect(new ObjectComparator().compare(actual, expected))
            .toEqual({ match: false, apply: true, diff: 'fail' })
    });
    it("should return value from jest toEqual", () => {
        when(diff).calledWith(actual, actual).mockReturnValue('not fail');

        expect(new ObjectComparator().compare(actual, actual))
            .toEqual({ match: true, apply: true, diff: 'not fail' })
    });
});
